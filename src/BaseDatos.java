
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author harce
 */
public class BaseDatos {
    
    private static ArrayList<Reclamo> values = new ArrayList();
    
    public static void agregarReclamo(Reclamo reclamo){
        values.add(reclamo);
    }
    
    public static boolean eliminarReclamo(String rut){
        boolean isDelete = false;        
        for(int x = 0; x < values.size(); x++){
            if(values.get(x).getRut().equals(rut)){
                values.remove(x);
                isDelete = true;
                break;
            }
        }
        return isDelete;
    }
    
    public static ArrayList<Reclamo> getValues(){
        return values;
    }
    
}
