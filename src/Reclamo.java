/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author harce
 */
public class Reclamo {
    
    private String nombre;
    private String rut;
    private String direccion;
    private String banco;
    private String tarjetaClonada;
    private String observacion;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getTarjetaClonada() {
        return tarjetaClonada;
    }

    public void setTarjetaClonada(String tarjetaClonada) {
        this.tarjetaClonada = tarjetaClonada;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @Override
    public String toString() {
        return "Nombre=" + nombre + ", RUT=" + rut + ", Dirección=" + direccion + ", Banco=" + banco + ", Tarjeta Clonada=" + tarjetaClonada + ", Observación=" + observacion + "\n";
    }
    
    
    
            
           
    
}
